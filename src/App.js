import React, {useEffect} from "react";
import PropTypes from 'prop-types';
import AppRoutes from "./components/routes/AppRoutes";
import axios from "axios";
import {connect} from "react-redux";


const App = (props) =>  {
    const {dispatch} = props;
    useEffect(() => {
            axios('http://localhost:3000/goods.json')
                .then(res => {
                    dispatch({type: 'SAVE_GOODS', payload: res.data })
                })}, [])

        return (
            <AppRoutes/>
        );

}

App.propTypes = {
   closeButton: PropTypes.bool,
   header: PropTypes.string,
   text: PropTypes.string,
 show: PropTypes.func
};

const mapStoreToProps = (store) => {
    return {
        goods: store.goods
    }
}


export default connect(mapStoreToProps)(App);
