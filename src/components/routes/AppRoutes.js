import React, {Component} from 'react';
import { Route } from 'react-router-dom';
import Nav from "../navigation/Nav";
import Cart from "../pages/cart/Cart";
import Fav from "../pages/fav/Fav";
import Main from "../pages/main/Main";

class AppRoutes extends Component {
    render() {
        return (
            <>
                <Nav/>
                <Route exact path='/' component={Main} />
                <Route path='/cart' component={Cart} />
                <Route path='/fav' component={Fav} />
            </>
        );
    }
}

export default AppRoutes;