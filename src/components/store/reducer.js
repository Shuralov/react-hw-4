const initialStore = {
    title: 'Asdf',
    show1: false,
    goods: [],
    starState: {}
}

const reducer = (store = initialStore, action) => {
    switch (action.type) {
        case "SET_SHOW":
            return {...store, show1: false}
        case "SET_SHOW_TRUE":
            return {...store, show1: true}
        case "STAR_STATE":
            return {...store, starState: action.payload}
        case "SAVE_GOODS":
            return {...store, goods: action.payload}
    }
    return store;
};

export default reducer;