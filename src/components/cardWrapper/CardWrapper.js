import React, {useState} from 'react';
import Card from "../card/Card";
import Button from "../button/Button";
import Modal from "../modal/Modal";
import "./CardWrapper.scss";
import {connect} from "react-redux";
const CardWrapper = (props) => {

    const {goods, show1, dispatch} = props;

    function store() {
        const values = [],
            keys = Object.keys(localStorage);
        let i = keys.length;
        while (i--) {
            values.push(localStorage.getItem(keys[i]));
        }
        return values
    }

    const cartAction = (para) =>
    {for (let i = 0; i < store().length; i++) {
        if (`${para}-bought` === store()[i]) {
            return true
        }
    }
    }

    const favAction = (para) =>
    { for (let i = 0; i < store().length; i++) {
        if (para === store()[i]) {
            return true
        }
    }
    }

    const mainAction = () =>
    { return true
    }

    const hideModals = (e) => {
        e.preventDefault();
        if(e.target === e.currentTarget) {
            dispatch({type: "SET_SHOW"})
        }
    };

    const starHandler = (i) => {
        dispatch({type: "STAR_STATE", payload:
                (currentState) => ({
                    [i.currentTarget.dataset.star_id]: !currentState[i.currentTarget.dataset.star_id],
                })
        })
        localStorage.getItem(i.currentTarget.dataset.star_id)?localStorage.removeItem(i.currentTarget.dataset.star_id):localStorage.setItem(i.currentTarget.dataset.star_id, i.currentTarget.dataset.star_id);
    }

    const modalController = (i) => {
        dispatch({type: "SET_SHOW_TRUE"})
        localStorage.setItem(`${i}-bought`, `${i}-bought`)
    }

    const modalController1 = (i) => {
                dispatch({type: "SET_SHOW_TRUE"})
                localStorage.removeItem(`${i}-bought`)
        }

    const captions = ['ok', 'cancel'];
    const actions = captions.map((e) =>
        <Button text={e} classname={'button'} modalController={hideModals}/>
    );

    return (
        <>
            <h1>{props.headerName}</h1>
            <div className={show1 ? 'modalOpened':'modalClosed'} onClick={hideModals}>
            <div className={"card-wrapper"}>
                {props.goods
                    .filter((good) => {
                        switch (props.action) {
                            case "FAVORITES":
                                return favAction(good.art)
                            case "CART":
                                return cartAction(good.art)
                            case "MAIN":
                                return mainAction()
                        }
                    })

                    .map((good) =>
                        <Card
                            cardName={good.name}
                            cardPrice={good.price}
                            cardArt={good.art}
                            cardColor={good.color}
                            img={good.img}
                            item={good.art}
                            starHandler={starHandler}
                            starId={good.art}
                        >
                            <Button key='2'
                                    text={props.buttonLabel}
                                    bg={'yellow'}
                                    modalController={ () => {
                                        switch (props.buttonAction) {
                                        case "FAVORITES":
                                        return modalController (good.art)
                                        case "CART":
                                        return modalController1 (good.art)
                                        }
                                    }
                                    }

                                    param={good.art}
                            />
                        </Card>)}
            </div>
            </div>
            <Modal key='1' modalController={hideModals}
                   show={show1}
                   closeButton={true}
                   header={props.modalTitle}>
                {actions}
            </Modal>
        </>
    );
}

const mapStoreToProps = (store) => {
    return {
        show1: store.show1,
        starState: store.starState,
        goods: store.goods
    }
}

export default connect(mapStoreToProps)(CardWrapper);