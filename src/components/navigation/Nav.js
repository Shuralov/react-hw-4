import React from 'react';
import {NavLink } from 'react-router-dom'
import "./Nav.scss";

const Nav = () => {
        return (
            <nav>
                <ul className={'links'}>
                    <li><NavLink exact activeClassName='is-active' to='/' className={'link-item'}>Main</NavLink></li>
                    <li><NavLink activeClassName='is-active' to='/fav' className={'link-item'}>Favorites</NavLink></li>
                    <li><NavLink activeClassName='is-active' to='/cart' className={'link-item'}>Cart</NavLink></li>
                </ul>
            </nav>
        );
}

export default Nav;