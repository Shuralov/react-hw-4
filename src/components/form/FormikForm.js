import React from 'react';
import {Field, Formik, Form, ErrorMessage} from 'formik';
import * as yup from 'yup';
import {connect} from "react-redux";
import "./FormikForm.scss";


const formSchema = yup.object().shape ({
    name: yup
        .string()
        .required()
        .min(2),
    lastname: yup
        .string()
        .required()
        .min(2),
    age: yup
        .number()
        .required()
        .min(2),
    address: yup
        .string()
        .required(),
    phone: yup
        .number()
        .required()
        .min(7)
})

const FormikForm = () =>  {

    function store() {
        const values = [],
            keys = Object.keys(localStorage);
        let i = keys.length;
        while (i--) {
            values.push(localStorage.getItem(keys[i]));
        }
        return values
    }

    const handleCheckout = (e) => {
    console.log('checkout: ' + store())
    console.log(e)
    localStorage.clear();
    }

    return (
            <Formik
                initialValues={{
                    name: '',
                    lastname: '',
                    age: '',
                    address: '',
                    phone: ''
                }}
                onSubmit={handleCheckout}
                validationSchema={formSchema}
            >
                {(formikProps) => {
                    return (
                        <Form onSubmit={formikProps.handleSubmit} noValidate className={'form'}>
                            <Field component='input' type='text' name='name' placeholder='John' className={'field'}/>
                            <ErrorMessage name='name'/>

                            <Field component='input' type='text' name='lastname' placeholder='Doe' className={'field'}/>
                            <ErrorMessage name='lastname'/>

                            <Field component='input' type='text' name='age' placeholder='Your age' className={'field'}/>
                            <ErrorMessage name='age'/>

                            <Field component='input' type='text' name='address' placeholder='Street' className={'field'}/>
                            <ErrorMessage name='address'/>

                            <Field component='input' type='text' name='phone' placeholder='+380' className={'field'}/>
                            <ErrorMessage name='phone'/>

                            <button type='submit'>Checkout</button>
                        </Form>
                    )}
                }
            </Formik>
    );
}

const mapStoreToProps = (store) => {
    return {
        show1: store.show1,
        starState: store.starState,
        goods: store.goods
    }
}
export default connect(mapStoreToProps)(FormikForm);