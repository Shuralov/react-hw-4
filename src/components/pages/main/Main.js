import React from 'react';
import {connect} from 'react-redux';
import CardWrapper from "../../cardWrapper/CardWrapper";

const Main = () => {

    return (
        <>
            <CardWrapper
                headerName={'Main'}
                action={'MAIN'}
                buttonLabel={'Add to cart'}
                buttonAction={'FAVORITES'}
                modalTitle={'Item is added to your cart'}
            />
        </>
    );
}

const mapStoreToProps = (store) => {
    return {
        show1: store.show1,
        starState: store.starState,
        goods: store.goods
    }
}

export default connect(mapStoreToProps)(Main);







