import React from 'react';
import {connect} from 'react-redux';
import CardWrapper from "../../cardWrapper/CardWrapper";
import FormikForm from "../../form/FormikForm";

const Cart = () => {

    return (
        <>
            <h1>Cart</h1>
            <FormikForm />
            <CardWrapper
                headerName={''}
                action={'CART'}
                buttonLabel={'Delete'}
                buttonAction={'CART'}
                modalTitle={'You have deleted an item'}
            />
        </>
    );
}

const mapStoreToProps = (store) => {
    return {
        show1: store.show1,
        starState: store.starState,
        goods: store.goods
    }
}

export default connect(mapStoreToProps)(Cart);