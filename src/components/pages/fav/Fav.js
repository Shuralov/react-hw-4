import React from 'react';
import {connect} from 'react-redux';
import CardWrapper from "../../cardWrapper/CardWrapper";

const Fav = () => {
    return (
        <>
          <CardWrapper
              headerName={'Favorites'}
              action={'FAVORITES'}
              buttonLabel={'Buy'}
              buttonAction={'FAVORITES'}
              modalTitle={'You have purchase an item'}
          />
        </>
    );
}

const mapStoreToProps = (store) => {
    return {
        show1: store.show1,
        starState: store.starState,
        goods: store.goods
    }
}

export default connect(mapStoreToProps)(Fav);