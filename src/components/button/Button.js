import React from 'react';
import "./Button.scss";

export const Button = (props) => {
        return (
            <button onClick={props.modalController}
                    style={{backgroundColor:props.bg}}
                    className={props.classname}
                    value={props.param}
            >
            {props.text}
            </button>
        );
}

export default Button;