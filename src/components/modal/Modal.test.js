import Modal from "./Modal";
import React from "react";
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom';
import "@testing-library/jest-dom/extend-expect";
import {getByTestId} from "@testing-library/dom";




describe('Testing Modal.js', () => {
    test('Button is rendered OK', () => {
            render(<Modal />)
    })

    test('Modal is in doc', () => {
        render (<Modal show={true}/>)
        const modal = document.querySelector('.modal')
        expect(modal).toBeInTheDocument()
    })

    test('Span has a class', () => {
        render (<Modal show={true}/>)
        const header = document.querySelector('h1')
        expect(header).toHaveClass('header')
    })
})





